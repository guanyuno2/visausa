# layoutVISA

#How to use

- To update or install library, we use "yarn" or "yarn install"

```
yarn install
```

- Development and run, we use "yarn dev" or "yarn watch"

```
yarn watch
```

- Build and deploy, we use "yarn prod"

```
yarn prod
```

- After building success, we have dest/ folder - exporting finally of running source code in browser